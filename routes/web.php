<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AdminController@login_page');
Route::get('/admin/logout', 'AdminController@logout');
Route::post('/admin/login', 'AdminController@login');

Route::get('/sampah', 'SampahController@index')->middleware('auth:admin');
Route::get('/sampah/edit/{id}', 'SampahController@edit')->middleware('auth:admin');
Route::get('/sampah/show_datatable', 'SampahController@show_datatable')->middleware('auth:admin');
Route::get('/sampah/json', 'SampahController@show_json')->middleware('auth:admin');
Route::post('/sampah/save', 'SampahController@save')->middleware('auth:admin');
Route::delete('/sampah/delete/{id}', 'SampahController@destroy')->middleware('auth:admin');

Route::get('/admin', 'AdminController@index')->middleware('auth:admin');
Route::get('/admin/show_datatable', 'AdminController@show_datatable')->middleware('auth:admin');
Route::get('/admin/edit/{id}', 'AdminController@edit')->middleware('auth:admin');
Route::post('/admin/save', 'AdminController@save')->middleware('auth:admin');
Route::put('/admin/reset_pass', 'AdminController@resetPasswords')->middleware('auth:admin');
Route::delete('/admin/delete/{id}', 'AdminController@delete')->middleware('auth:admin');
