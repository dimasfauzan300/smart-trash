<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function index()
    {
        $data['title'] = "Data Admin";
        $data['active'] = "admin";
        return view('admin.admin_view', $data);
    }

    public function login_page()
    {
        return view('admin.loginForm');
    }

    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        if (Auth::guard('admin')->attempt(['username' => $username, 'password' => $password])) {
            return redirect()->intended('/sampah');
        }else{
            $request->session()->flash('message', 'fail');
            return redirect('admin');
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }

    public function show_datatable()
    {
        $data = Admin::select(['id', 'username', 'nama', 'created_at']);
        return DataTables()->of($data)
                ->editColumn('created_at', function($data){
                    return date('d-m-Y H:i:s', strtotime($data->created_at));
                })
                ->addColumn('action', function($data){
                    return '<a href="#" class="btn btn-info" data-id="'.$data->id.'" data-toggle="modal" data-target="#modelId" id="buton_edit"><i class="fa fa-edit"></i> Edit</a> '.
                    '<a href="#" class="btn btn-danger" data-id="'.$data->id.'" id="buton_hapus"><i class="fa fa-trash"></i> Hapus</a> '.
                    '<a href="#" data-toggle="modal" data-target="#modelPassword" class="btn btn-warning" data-id="'.$data->id.'" id="buton_reset"><i class="fa fa-sync-alt"></i> Reset PWD</a>';
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
    }

    public function edit($id)
    {
        $sampah = Admin::where(['id' => $id])->first();
        return response()->json($sampah);
    }

    public function save(Request $request)
    {
        $password = Hash::make($request->password);
        $data = [];
        if($request->jenis == 'tambah')
        {
            $data = [
                'username' => $request->username,
                'password' => $password,
                'nama' => $request->nama,
            ];
        }else{
            $data = [
                'username' => $request->username,
                'nama' => $request->nama,
            ];
        }
        Admin::updateOrCreate(['id' => $request->id], $data);
        return response()->json(['succsess'], 201);
    }

    public function resetPasswords(Request $request)
    {
        $password = Hash::make($request->password);
        Admin::where(['id' => $request->id])->update([
            'password' => $password,
        ]);
        return response()->json(['succsess'], 200);
    }

    public function delete($id)
    {
        Admin::destroy($id);
        return response()->json(['succsess'], 204);
    }
}
