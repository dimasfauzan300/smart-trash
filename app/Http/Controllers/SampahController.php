<?php

namespace App\Http\Controllers;

use App\Sampah;
use Illuminate\Http\Request;

class SampahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Data Sampah";
        $data['active'] = "sampah";
        return view('sampah.sampah_view', $data);
    }

    public function save(Request $request)
    {
        $data = [];
        if($request->id != null)
        {
             $data = [
                'alamat' => $request->alamat,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'status_tong' => 1
            ];
            Sampah::updateOrCreate(['id' => $request->id], $data);
        }else{
            $jml_tong = Sampah::select('id')->count() + 1;
            $id = 'TS-'.$jml_tong;
            $data = [
                'alamat' => $request->alamat,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'status_tong' => 1
            ];
            Sampah::updateOrCreate(['id' => $id], $data);
        }

        return response()->json(['succsess'], 201);
    }

    public function save_api(Request $request)
    {
        $data = [
            'status_tong' => $request->status_tong
        ];
        Sampah::findOrFail($request->id)->update($data);
        return response()->json(['succsess'], 200);
    }


    public function show_json(Sampah $sampah)
    {
        $sampah = Sampah::select(['id', 'alamat', 'latitude', 'longitude', 'status_tong'])->get();
        return response()->json($sampah, 200);
    }

    public function show_datatable()
    {
        $data = Sampah::select(['id', 'alamat', 'latitude', 'longitude', 'status_tong', 'created_at'])->orderBy('status_tong', 'desc');
        return DataTables()->of($data)
                ->editColumn('created_at', function($data){
                    return date('d-m-Y H:i:s', strtotime($data->created_at));
                })
                ->editColumn('status_tong', function($data){
                    if($data->status_tong == 0)
                    {
                        return "Kosong";
                    }else if($data->status_tong == 1){
                        return "ID Belum Terpasang";
                    }else{
                        return "Tong Sampah Penuh";
                    }
                })
                ->addColumn('action', function($data){
                    return '<a href="#" class="btn btn-info" data-id="'.$data->id.'" id="buton_edit"><i class="fa fa-edit"></i> Edit</a> '.
                    '<a href="#" class="btn btn-danger" data-id="'.$data->id.'" id="buton_hapus"><i class="fa fa-trash"></i> Hapus</a> ';
                })
                ->rawColumns(['action', 'status_tong'])
                ->addIndexColumn()
                ->make(true);
    }

    public function edit($id)
    {
        $sampah = Sampah::select(['alamat', 'latitude', 'longitude', 'status_tong'])->where('id', $id)->first();
        return response()->json($sampah, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sampah  $sampah
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sampah::destroy($id);
        return response()->json(['succsess'], 204);
    }
}
