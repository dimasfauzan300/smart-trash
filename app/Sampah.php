<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sampah extends Model
{
    protected $table = "tbl_sampah";

    protected $fillable = ['id', 'alamat', 'latitude', 'longitude', 'status_tong'];

    protected $primaryKey = "id";

    protected $casts = ['id' => 'string'];

    public $incrementing = false;
}
