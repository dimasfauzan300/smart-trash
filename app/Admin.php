<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    protected $table = "admin";

    protected $fillable = ['username', 'password', 'nama'];

    protected $primaryKey = "id";

    protected $hidden = ['password'];

}
