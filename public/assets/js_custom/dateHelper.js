function changeDate(date){
    date_temp=date.split('-');
    return date_temp[2]+' '+changeBulan(date_temp[1])+' '+date_temp[0];
}

function changeBulan(bulan)
{
    switch(bulan){
        case '01' : return 'Januari';
        case '02' : return 'Februari';
        case '03' : return 'Maret';
        case '04' : return 'April';
        case '05' : return 'Mei';
        case '06' : return 'Juni';
        case '07' : return 'Juli';
        case '08' : return 'Agustus';
        case '09' : return 'September';
        case '10' : return 'Oktober';
        case '11' : return 'November';
        case '12' : return 'Desember';
    }
}
