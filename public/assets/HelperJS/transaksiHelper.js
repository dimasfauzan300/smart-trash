function HitungTotalBayar()
{
    let Total = 0;
    $('#tableTransaksi tbody tr').each(function(){
        let SubTotal = $(this).find('td:nth-child(5) input').val();
        if(SubTotal > 0)
        {
            Total = parseInt(Total) + parseInt(SubTotal);
        }
    });
    return Total;
}

function setTotalBayar(Total)
{
    $('#TotalBayar').html(to_rupiah(Total));
    $('#TotalBayarHidden').val(Total);
    $('#UangCash').val('');
    $('#UangKembali').val('');
}

function to_rupiah(angka){
    var rev = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2 = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return 'Rp. ' + rev2.split('').reverse().join('');
}
