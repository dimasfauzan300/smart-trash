<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSampahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_sampah', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->text('alamat');
            $table->string('latitude', 100);
            $table->string('longitude', 100);
            $table->integer('status_tong');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_sampah');
    }
}
