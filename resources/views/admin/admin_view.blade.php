@section('custom_css')
<link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.min.css')}}">
@endsection
@extends('layout.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Data Admin</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 mb-3 d-flex justify-content-end">
                                  <!-- Button trigger modal -->
                                  <button type="button" class="btn btn-primary btn-md" data-toggle="modal"
                                      data-target="#modelId" id="tombol-tambah">
                                      <i class="fa fa-plus"></i> Tambah Data
                                  </button>
                                </div>
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="table-data" style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ID</th>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Nama</th>
                                                    <th class="text-center">Username</th>
                                                    <th class="text-center">Created At</th>
                                                    <th class="text-center">Option</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="#" id="data-formulir">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Data Jenis Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="jenis_menu">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama" required>
                    </div>
                    <div class="form-group">
                        <label for="jenis_menu">Username</label>
                        <input type="text" name="username" class="form-control" id="username" required>
                    </div>
                    <div class="form-group" id="pass_v">
                        <label for="jenis_menu">Password</label>
                        <input type="password" name="password" class="form-control" id="password" required>
                    </div>
                    <input type="hidden" name="id" id="id" value="">
                    <input type="hidden" name="jenis" id="jenis" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" id="simpan-data">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modelPassword" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="#" id="data-pass">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Data Reset Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="jenis_menu">Password</label>
                        <input type="password" name="password" class="form-control" id="password" required>
                    </div>
                    <input type="hidden" name="id" id="id" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" id="simpan-data-pass">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script_custom')
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert2/sweetalert2.all.min.js')}}"></script>
<script>
    $(document).ready(function () {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        function getData() {
            $('#table-data').DataTable({
                processing: true,
                serverSide: true,
                ordering: true,
                ajax: {
                    url: `{{url('/admin/show_datatable')}}`,
                    type: 'GET'
                },
                columns: [
                    {data: 'id',name: 'id',visible: false,searchable: false,className: 'text-center'},
                    {data: 'DT_RowIndex',name: 'DT_RowIndex',orderable: false,searchable: false,className: 'text-center'
                    },
                    {data: 'nama',name: 'nama',className: 'text-center'},
                    {data: 'username',name: 'username',className: 'text-center'},
                    {data: 'created_at',name: 'created_at',searchable: false,className: 'text-center'},
                    {data: 'action',name: 'action',searchable: false,orderable: false,className:'text-center'},
                ]
            });
            $("#table-data_filter").addClass('d-flex justify-content-end');
            $("#table-data_filter>label>input").removeClass('form-control-sm');
            $("[name=table-data_length]").removeClass('form-control-sm');
            $("[name=table-data_length]").removeClass('custom-select-sm');
        }

        function reset()
        {
            $("#nama").val('');
            $("#username").val('');
            $("#password").val('');
            $("#id").val('');
        }

        getData();

        $("#tombol-tambah").on('click', function (e) {
            e.preventDefault();
            $(".modal-title").html("Tambah Data Admin");
            $("#pass_v").show();
            $("#jenis").val('tambah');
            reset();
        });

        $(this).on('click', '#buton_reset', function (e) {
            let id=$(this).data('id');
            $("#id").val(id);
        });

        $(this).on('click',"#buton_edit",function (e) {
            e.preventDefault();
            $("#pass_v").hide();
            $("#jenis").val('edit');
            let id=$(this).data('id');
            $.ajax({
                type: "get",
                url: `{{url('admin/edit')}}/${id}`,
                dataType: "json",
                success: function (response) {
                    $("#nama").val(response.nama);
                    $("#username").val(response.username);
                    $("#id").val(id);
                },
                error: function(){
                    Toast.fire({
                        type: 'error',
                        title: 'Gagal mengambil data !'
                    })
                }
            });
            $(".modal-title").html("Ubah Data Admin");
        });

        $(this).on('click', '#simpan-data-pass', function (e) {
            e.preventDefault();
            $('#simpan-data-pass').html("Menyimpan...");
            $('#simpan-data-pass').attr('disabled',true);
            let data = $("#data-pass").serialize();
            $.ajax({
                type: "put",
                url: "{{url('admin/reset_pass')}}",
                data: data,
                dataType: "json",
                success: function (response) {
                    reset();
                    $('#simpan-data-pass').html("Simpan");
                    $('#simpan-data-pass').removeAttr('disabled');
                    $("#modelPassword").modal('hide');
                    let oTable = $('#table-data').dataTable();
                    oTable.fnDraw(false);
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil menyimpan data !'
                    });
                },
                error:function(e)
                {
                    Toast.fire({
                        type: 'error',
                        title: 'Gagal menyimpan data !'
                    });
                    return;

                }
            });

        });

        $(this).on('click', '#buton_hapus', function (e) {
            e.preventDefault();
            let id=$(this).data('id');
            Swal.fire({
                title: 'Peringatan',
                text: "Apakah anda yakin akan menghapus data ?",
                type: 'warning',
                showCancelButton: true,
                buttonsStyling:false,
                confirmButtonClass:'btn btn-danger btn-lg mr-2',
                cancelButtonClass:'btn btn-default btn-lg',
                confirmButtonText: '<i class="fa fa-trash"></i> Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: `{{url('/admin/delete')}}/${id}`,
                        data: {
                            _token:'{{csrf_token()}}'
                        },
                        dataType: "json",
                        success: function (response) {
                            let oTable = $('#table-data').dataTable();
                            oTable.fnDraw(false);
                        },
                        error:function()
                        {
                            Toast.fire({
                                type: 'error',
                                title: 'Gagal menghapus data !'
                            })
                        }
                    });
                }
            })
        });

        $('#simpan-data').on('click', function (e) {
            e.preventDefault();
            $('#simpan-data').html("Menyimpan...");
            $('#simpan-data').attr('disabled',true);
            let data = $("#data-formulir").serialize();
            $.ajax({
                type: "post",
                url: "{{url('admin/save')}}",
                data: data,
                dataType: "json",
                success: function (response) {
                    reset();
                    $('#simpan-data').html("Simpan");
                    $('#simpan-data').removeAttr('disabled');
                    $("#modelId").modal('hide');
                    let oTable = $('#table-data').dataTable();
                    oTable.fnDraw(false);
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil menyimpan data !'
                    });
                },
                error:function(e)
                {
                    Toast.fire({
                        type: 'error',
                        title: 'Gagal menyimpan data !'
                    });
                    return;

                }
            });
        });
    });
</script>
@endsection
