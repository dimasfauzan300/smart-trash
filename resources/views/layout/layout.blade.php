<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>{{$title}}</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('assets/dist/css/adminlte.min.css')}}">
  <link rel="shortcut icon" href="{{asset('assets/dist/img/favicon.png')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  @yield('custom_css')
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper">

  @include('layout.navbar')

  @auth('admin')
    @include('layout.sidebar')
  @endauth


  @yield('content')

  @include('layout.controlSidebar')

  @include('layout.footer')

</div>
<!-- ./wrapper -->

@include('layout.scriptJS')

@yield('script_custom')
</body>
</html>
