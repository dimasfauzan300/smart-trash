<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{asset('assets/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('upload_img/kasir/'.Auth::guard('kasir')->user()->berkasFoto)}}" class="img" alt="User Image" style="">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::guard('kasir')->user()->namaLengkap }}</a>
                <small class="d-block white" style="color:white;">{{ Auth::guard('kasir')->user()->nip }}</small>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a href="{{url('transaksi')}}" class="nav-link @if ($active == 'transaksi') active @endif">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>
                            Transaksi
                        </p>
                    </a>
                </li>
                <li
                    class="nav-item has-treeview @if($active == 'memberAktif' || $active == 'memberNonaktif') menu-open @endif">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Data Member
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/member/aktif') }}"
                                class="nav-link @if($active == 'memberAktif') active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Data Member Aktif</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/member/nonaktif') }}"
                                class="nav-link @if($active == 'memberNonaktif') active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Data Member Nonaktif</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{url('kasir/logout')}}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Keluar
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
