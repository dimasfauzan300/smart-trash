@section('custom_css')
<link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.min.css')}}">
@endsection
@extends('layout.layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-sm-8">
                  <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Sebaran Tong Sampah</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12" id="map" style="height:500px"></div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Form Tong Sampah</h3>
                    </div>
                    <div class="card-body">
                        <form id="form_tong_sampah">
                            @csrf
                            <input type="hidden" name="id" id="id">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-9">
                                    <textarea name="alamat" id="alamat" cols="10" rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Latitude</label>
                                <div class="col-sm-9">
                                    <input type="text" name="latitude" class="form-control" id="latitude">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Longitude</label>
                                <div class="col-sm-9">
                                    <input type="text" name="longitude" class="form-control" id="longitude">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info float-right" id="tombol_simpan"><i class="fa fa-save"></i> Simpan</button>
                        <button type="submit" class="btn btn-warning float-right mr-2" id="tombol_reset"><i class="fa fa-sync-alt"></i> Reset</button>
                        <button type="submit" class="btn btn-success float-right mr-2" id="tombol_tambah"><i class="fa fa-plus"></i> Tambah</button>
                        <button type="submit" class="btn btn-default float-right mr-2" id="tombol_cancel"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </div>
              </div>
          </div>
          <div class="row">
              <div class="col-sm-12">
                  <div class="card">
                      <div class="card-header">
                          <h3 class="card-title">Data Tong Sampah</h3>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive">
                              <table class="table table-bordered" id="table-data" style="width: 100%">
                                  <thead>
                                      <tr>
                                          <th class="text-center" width="30px">#</th>
                                          <th class="text-center" width="30px">ID</th>
                                          <th class="text-center">Alamat</th>
                                          <th class="text-center">Lat</th>
                                          <th class="text-center">Long</th>
                                          <th class="text-center" width="80px">Keterangan</th>
                                          <th class="text-center">Created At</th>
                                          <th class="text-center">Opsi</th>
                                      </tr>
                                  </thead>
                                  <tbody></tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
  @section('script_custom')
  <script src="{{asset('assets/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.all.min.js')}}"></script>
  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyB6Rcjib0SGx3E4LNaQRXBP_rSBC7ktsdM&sensor=true"></script>

  <script>
    $(document).ready(function () {

    });
    var markers = [];
    var peta;

    function initialize() {
        var propertiPeta = {
            center: new google.maps.LatLng(-6.558185, 107.447167),
            zoom: 13,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };

        peta = new google.maps.Map(document.getElementById("map"), propertiPeta);

        hideSimpanButton();

        showAllMarkerTongSampah();

        $('#table-data').DataTable({
            processing: true,
            serverSide: true,
            ordering: true,
            ajax: {
                url: `{{url('/sampah/show_datatable')}}`,
                type: 'GET',
                async: false
            },
            columns: [
                {data: 'DT_RowIndex',name: 'DT_RowIndex',orderable: false,searchable: false,className: 'text-center'
                },
                {data: 'id',name: 'id',visible: true,searchable: false,className: 'text-center'},
                {data: 'alamat',name: 'alamat',className: 'text-center'},
                {data: 'latitude',name: 'latitude',className: 'text-center'},
                {data: 'longitude',name: 'longitude',className: 'text-center'},
                {data: 'status_tong',name: 'status_tong',className: 'text-center'},
                {data: 'created_at',name: 'created_at',searchable: false,className: 'text-center'},
                {data: 'action',name: 'action',searchable: false,orderable: false,className:'text-center'},
            ]
        });
        $("#table-data_filter").addClass('d-flex justify-content-end');
        $("#table-data_filter>label>input").removeClass('form-control-sm');
        $("[name=table-data_length]").removeClass('form-control-sm');
        $("[name=table-data_length]").removeClass('custom-select-sm');

    }

    function setMapOnAll(map) {
        for (let i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
    }

    function clearMarkers() {
        setMapOnAll(null);
    }


    function hideSimpanButton() {
        $("#form_tong_sampah input").attr('disabled', 'true');
        $("#alamat").attr('disabled', 'true');
        $('#tombol_simpan').hide();
        $('#tombol_reset').hide();
        $('#tombol_cancel').hide();
        $('#tombol_tambah').show();
    }

    function showSimpanButton() {
        $("#form_tong_sampah input").removeAttr('disabled');
        $("#alamat").removeAttr('disabled');
        $('#tombol_simpan').show();
        $('#tombol_reset').show();
        $('#tombol_cancel').show();
        $('#tombol_tambah').hide();

    }

    // event jendela di-load
    google.maps.event.addDomListener(window, 'load', initialize);


    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    function showAllMarkerTongSampah()
    {
        setMapOnAll(null);
        $.ajax({
            type: "get",
            url: "{{url('sampah/json')}}",
            dataType: "json",
            success: function (response) {
                $.each(response, function (i, val) {
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(val.latitude, val.longitude),
                        map: peta,
                        animation: google.maps.Animation.BOUNCE,
                        title: `${val.id}`
                    });

                    let contentString = `<table class="table table-borderless">`;
                    contentString += `<tr>`;
                    contentString += `<td>ID</td>`;
                    contentString += `<td>: ${val.id}</td>`;
                    contentString += `</tr>`;
                    contentString += `<tr>`;
                    contentString += `<td>Alamat</td>`;
                    contentString += `<td>: ${val.alamat}</td>`;
                    contentString += `</tr>`;
                    contentString += `<tr>`;
                    contentString += `<td>Lat</td>`;
                    contentString += `<td>: ${val.latitude}</td>`;
                    contentString += `</tr>`;
                    contentString += `<tr>`;
                    contentString += `<td>Lng</td>`;
                    contentString += `<td>: ${val.longitude}</td>`;
                    contentString += `</tr>`;
                    contentString += `<tr>`;
                    contentString += `<td>Keterangan</td>`;
                    if(val.status_tong == '0'){
                        contentString += `<td>: Kosong</td>`;
                    }else if(val.status_tong == '1'){
                        contentString += `<td>: Belum ada device</td>`;
                    }else if(val.status_tong == '2'){
                        contentString += `<td>: Penuh</td>`;
                    }

                    contentString += `</tr>`;
                    contentString += `</table>`;

                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });

                    google.maps.event.addListener(marker, 'click', function(event) {
                        infowindow.open(map, marker);
                    });
                    markers.push(marker);
                });
            }
        });
    }


    function reset()
    {
        $("#alamat").val('');
        $("#latitude").val('');
        $("#longitude").val('');
        $("#id").val('');
    }

    $(document).on('click', '#tombol_tambah', function (e) {
        e.preventDefault();
        showSimpanButton();
        setMapOnAll(null);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(-6.558185, 107.447167),
            map: peta,
            draggable:true,
            animation: google.maps.Animation.BOUNCE
        });

        google.maps.event.addListener(marker, 'dragend', function(event) {
            $("#latitude").val(event.latLng.lat().toString());
            $("#longitude").val(event.latLng.lng().toString());
        });
        markers.push(marker);
    });

    $(document).on('click', '#tombol_reset', function (e) {
        e.preventDefault();
        reset();
    });

    $(document).on('click', '#tombol_cancel', function (e) {
        e.preventDefault();
        hideSimpanButton();
        showAllMarkerTongSampah();
    });

    $(document).on('click', '#tombol_simpan', function (e) {
        e.preventDefault();
        $('#tombol_simpan').html("Menyimpan...");
        $('#tombol_simpan').attr('disabled', true);
        let data = $("#form_tong_sampah").serialize();
        $.ajax({
            type: "post",
            url: "{{url('sampah/save')}}",
            data: data,
            dataType: "json",
            success: async function (response) {
                setMapOnAll(null);
                $('#tombol_simpan').html("Simpan");
                $('#tombol_simpan').removeAttr('disabled');
                let oTable = $('#table-data').dataTable();
                oTable.fnDraw(false);
                reset();
                await showAllMarkerTongSampah();
                hideSimpanButton();
                Toast.fire({
                    type: 'success',
                    title: 'Berhasil menyimpan data !'
                });
            },
            error: function(e)
            {
                $('#tombol_simpan').html("Simpan");
                $('#tombol_simpan').removeAttr('disabled');
                Toast.fire({
                    type: 'error',
                    title: 'Gagal menyimpan data !'
                });
            }
        });
    });

    $(document).on('click', "#buton_edit", function (e) {
        e.preventDefault();
        showSimpanButton();
        let id=$(this).data('id');
        $.ajax({
            type: "get",
            url: `{{url('sampah/edit')}}/${id}`,
            dataType: "json",
            success: function (response) {
                $("#alamat").val(response.alamat);
                $("#latitude").val(response.latitude);
                $("#longitude").val(response.longitude);
                $("#id").val(id);
                setMapOnAll(null);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(response.latitude, response.longitude),
                    map: peta,
                    draggable:true,
                    animation: google.maps.Animation.BOUNCE
                });
                google.maps.event.addListener(marker, 'dragend', function(event) {
                    $("#latitude").val(event.latLng.lat().toString());
                    $("#longitude").val(event.latLng.lng().toString());
                });
                markers.push(marker);
            },
            error: function(){
                Toast.fire({
                    type: 'error',
                    title: 'Gagal mengambil data !'
                })
            }
        });
    });

    $(document).on('click', '#buton_hapus', function (e) {
        e.preventDefault();
        let id=$(this).data('id');
        Swal.fire({
            title: 'Peringatan',
            text: "Apakah anda yakin akan menghapus data ?",
            type: 'warning',
            showCancelButton: true,
            buttonsStyling:false,
            confirmButtonClass:'btn btn-danger btn-lg mr-2',
            cancelButtonClass:'btn btn-default btn-lg',
            confirmButtonText: '<i class="fa fa-trash"></i> Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    url: `{{url('/sampah/delete')}}/${id}`,
                    data: {
                        _token:'{{csrf_token()}}'
                    },
                    dataType: "json",
                    success: async function (response) {
                        let oTable = $('#table-data').dataTable();
                        oTable.fnDraw(false);
                        await showAllMarkerTongSampah();
                        Toast.fire({
                            type: 'success',
                            title: 'Berhasil menghapus data !'
                        });
                    },
                    error:function()
                    {
                        Toast.fire({
                            type: 'error',
                            title: 'Gagal menghapus data !'
                        })
                    }
                });
            }
        })
    });
  </script>
  @endsection

